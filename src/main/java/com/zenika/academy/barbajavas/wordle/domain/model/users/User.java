package com.zenika.academy.barbajavas.wordle.domain.model.users;

import org.springframework.util.StringUtils;

import javax.persistence.*;

@Entity
@Table(name="users")
@Access(AccessType.FIELD)
public class User {
    @Id
    private String tid;
    @Column
    private String email;
    @Column
    private String username;

    protected User() {

    }

    public User(String tid, String email, String username) {
        this.tid = tid;
        this.username = username;
        this.email = email;
    }

    // On crée un constructeur vide pour utiliser spring jpa


    public String getTid() {
        return tid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        if(!StringUtils.hasText(username)) {
            throw new IllegalArgumentException("Username can't be null");
        }
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!StringUtils.hasText(email)) {
            throw new IllegalArgumentException("Email can't be null");
        }
        this.email = email;
    }
}
