create table if not exists users(
    tid char(36) primary key,
    email text not null,
    name text
);

create table if not exists games (
    tid char(36) primary key,
    word text not null,
    max_attempts int not null,
    user_tid char(36) REFERENCES users(tid)
);
-- Ne pas oublier d'intégrer le lien qui établit la connexion entre les tables

create table if not exists rounds (
    word text,
    round_order int,
    game_tid char(36) REFERENCES games(tid),
    primary key(round_order, game_tid)
);

-- from users left join game on game.user_tid = users.tid